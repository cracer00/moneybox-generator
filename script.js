let table = "<table><tr>";

function createMoneyboxArray(sum, from, to) {
    let arr = [];
    document.querySelector('#moneybox').innerHTML = '';
    for (let i = sum; i > 1;){
        let oneBox = getRandom(from, to);
        arr.push(oneBox);
        console.log(oneBox);
        i -= oneBox;
        console.log("Осталось: " + i);
    }
    return arr;
}

let tableMarkdown = function(arr, generateCells) {
    let arrSum = 0;
    arr.forEach((value, i) => {
        table += `<td>${value}</td>`;
        let a = i + 1;
        if (a % generateCells === 0 && a !== arr.length) {
            table += "</tr><tr>";
        }
        arrSum += value;
    });
    return arrSum;
}

function dataTrim(str) {
    str.replace(/[^+\d]/g, '');
    let int = parseInt(str);
    console.log(int);
    return int;
}

function createMoneybox() {
    document.querySelector('#moneybox_info').innerHTML = ``;

    let sumToGenerate = document.querySelector('#settings_input');
    sumToGenerate = dataTrim(sumToGenerate.value);

    let generateFrom = document.querySelector('#settings_input__from');
    generateFrom = dataTrim(generateFrom.value);

    let generateTo = document.querySelector('#settings_input__to');
    generateTo = dataTrim(generateTo.value);

    let generateCells = document.querySelector('#settings_input__cells');
    generateCells = dataTrim(generateCells.value);

    let arr = createMoneyboxArray(sumToGenerate, generateFrom, generateTo);
    let arrayGlobalSum = tableMarkdown(arr, generateCells);
    table += "</tr></table>";
    document.querySelector('.moneybox').innerHTML = table;
    document.querySelector('.moneybox_info').innerHTML += `
            <div>Сумма: ${arrayGlobalSum}</div>`;
    table = "<table><tr>";
}

function deleteInst(){
    document.querySelector('.moneybox').innerHTML = ``;
}

function getRandom(min, max) {
        let arrRand = [100, 200, 300, 400, 500];
        let res = Math.trunc(Math.random() * (max - min) + min);
        let str = res + '';
        if (str.endsWith('0') === true) {
            return res;
        } else {
            return arrRand[Math.floor(Math.random()*arrRand.length)];
        }
}

document.querySelector('.start_button').addEventListener('click', () => {
    createMoneybox();
});

document.querySelector('.delete_button').addEventListener('click', () => {
    deleteInst();
});





